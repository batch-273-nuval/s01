/*
	Quiz:

	1. How do you create arrays in JS?
		- declare via a variable or using array literal using square brackets to enclose the items

	2. How do you access the first character of an array?
		- use the square bracket and put inside it the index number which is zero example array name is array and here is the syntax: array[0]

	3. How do you access the last character of an array?
		- use the .length method to get length of array then you subtract it by 1 to get the last index number and here is the syntax: array[array.length -]

	4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
		- indexOf() Method
		
	5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
		- Iterator Methods - forEach() Method
		
	6. What array method creates a new array with elements obtained from a user-defined function?
		- Iterator Methods - map() Method
		
	7. What array method checks if all its elements satisfy a given condition?
		- Iterator Methods - every() Method
		
	8. What array method checks if at least one of its elements satisfies a given condition?
		- some() Method
		
	9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
		- True
		
	10.True or False: array.slice() copies elements from original array and returns them as a new array.
		- True
		
*/

// Function Coding

let students = ["John", "Joe", "Jane", "Jessie"];

// 1.

let name = "";

function addToEnd(students, name) {
	if (typeof name !== "string") {
		return "error - can only add strings to an array";
	} else {
		students.push(name);
		return students;
	}
	
  }


// 2.

function addToStart(students, name) {
	if (typeof name !== "string") {
		return "error - can only add strings to an array";
	} else {
		students.unshift(name);
		return students;
	}
	
  }


// 3.

function elementChecker(array, name) {
  if (array.length === 0) {
    return "error - passed in array is empty";
  } else {
    return array.includes(name);
  }
}


// 4. 

  function checkAllStringsEnding(students, char) {
  if (students.length === 0) {
    return "error - array must NOT be empty";
  }
  if (!students.every((s) => typeof s === "string")) {
    return "error - all array elements must be strings";
  }
  if (typeof char !== "string") {
    return "error - 2nd argument must be of data type string";
  }
  if (char.length !== 1) {
    return "error - 2nd argument must be a single character";
  }

  return students.every((s) => s.endsWith(char));
}


// 5.

function stringLengthSorter(students) {
  let stringArray = students.every((element) => {
    return typeof element === "string";
  });

  if (stringArray) {
    const ascending = students.sort((x, y) => x.length - y.length);
    return ascending;
  } else {
    return "error - all array elements must be strings";
  }
}


// 6.

function startsWithCounter(students, char) {
  if (students.length === 0) {
    return "error - array must NOT be empty";
  }
  if (!students.every((value) => typeof value === "string")) {
    return "error - all array elements must be strings";
  }
  if (typeof char !== "string") {
    return "error - 2nd argument must be of data type string";
  }
  if (char.length !== 1) {
    return "error - 2nd argument must be a single character";
  }

  let count = 0;
  for (let x = 0; x < students.length; x++) {
    if (students[x].toLowerCase().startsWith(char.toLowerCase())) {
      count++;
    }
  }
  return count;
}